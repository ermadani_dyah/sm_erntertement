<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\album;
class AlbumController extends Controller
{
    public function index(){
        // $album=DB::table('album')->get();
        $album=album::all();
        return view('admin.content.album',compact('album'));
    }
    public function cari(Request $request){
		// menangkap data pencarian
		$cari = $request->cari;
 
        // mengambil data dari table pegawai sesuai pencarian data
        // $album=DB::table('album')->where('nama_album','like',"%".$cari."%")->get();
        $album=album::where('nama_album','like',"%".$cari."%")->get();
        // mengirim data pegawai ke view index
        return view('admin.content.album',compact('album'));
	}
    public function tambah(){
        return view('admin.content.add.add_album');
    }
    public function store(Request $request){
        $request->validate([
            'nama_album'=>'required|unique:album',
            'nama_penyanyi'=>'required',
            'album_type'=>'required',
            'genre'=>'required',
            'record_label'=>'required',
            'distribution'=>'required',
            'release'=>'required',
            'gambar'=>'required',
            'link'=>'required',
        ]);
        // $query=DB::table('album')->insert(
        //     [
        //         "nama_album"=>$request['nama_album'],
        //         "nama_penyanyi"=>$request['nama_penyanyi'],
        //         "album_type"=>$request['album_type'],
        //         "genre"=>$request['genre'],
        //         "record_label"=>$request['record_label'],
        //         "distribution"=>$request['distribution'],
        //         "release"=>$request['release'],
        //         "gambar"=>$request['gambar'],
        //         "link"=>$request['link'],
        //     ]
        // );
        $album=album::create([
            "nama_album"=>$request['nama_album'],
            "nama_penyanyi"=>$request['nama_penyanyi'],
            "album_type"=>$request['album_type'],
            "genre"=>$request['genre'],
            "record_label"=>$request['record_label'],
            "distribution"=>$request['distribution'],
            "release"=>$request['release'],
            "gambar"=>$request['gambar'],
            "link"=>$request['link'],
        ]);
        return redirect('/admin/album')->with('sukses','Yee selamat data Berhasil Disimpan');
    }
    public function detail($id){
        // $album=DB::table('album')->where('id',$id)->first();
        $album=album::find($id);
        return view('admin.content.detail.detail_album',compact('album'));
    }
    public function delate($id){
        // $query=DB::table('album')->where('id',$id)->delete();
        album::destroy($id);
        return redirect('/admin/album')->with('sukses','Data Anda Berhasil Dihapus');
    }
    public function edit($id){
        // $album=DB::table('album')->where('id',$id)->first();
        $album=album::find($id);
        return view('admin.content.edit.edit_album',compact('album'));
    }
    public function update($id,Request $request){
        $request->validate([
            'nama_album'=>'required|unique:album',
            'nama_penyanyi'=>'required',
            'album_type'=>'required',
            'genre'=>'required',
            'record_label'=>'required',
            'distribution'=>'required',
            'release'=>'required',
            'gambar'=>'required',
            'link'=>'required',
        ]);
        // $query=DB::table('album')->where('id',$id)->update([
        //     "nama_album"=>$request['nama_album'],
        //     "nama_penyanyi"=>$request['nama_penyanyi'],
        //     "album_type"=>$request['album_type'],
        //     "genre"=>$request['genre'],
        //     "record_label"=>$request['record_label'],
        //     "distribution"=>$request['distribution'],
        //     "release"=>$request['release'],
        //     "gambar"=>$request['gambar'],
        //     "link"=>$request['link'],
        // ]);
        $update=album::where('id',$id)->update([
            "nama_album"=>$request['nama_album'],
            "nama_penyanyi"=>$request['nama_penyanyi'],
            "album_type"=>$request['album_type'],
            "genre"=>$request['genre'],
            "record_label"=>$request['record_label'],
            "distribution"=>$request['distribution'],
            "release"=>$request['release'],
            "gambar"=>$request['gambar'],
            "link"=>$request['link'],
        ]);
        return redirect('/admin/album')->with('sukses','Yee selamat data Berhasil Diupdate');
    }
}
