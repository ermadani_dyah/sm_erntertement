<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\team;
use App\kategori_teams;
class TeamController extends Controller
{
    public function index(){
        // $team=DB::table('team')->get();
        // $kategori_team=DB::table('kategori_teams')->get();
        $team=team::all();
        $kategori_team=kategori_teams::all();
        return view('admin.content.team',compact('team','kategori_team'));
    }
    public function cari(Request $request){
		// menangkap data pencarian
		$cari = $request->cari;
 
        // mengambil data dari table pegawai sesuai pencarian data
        // $team=DB::table('team')->where('nama_lengkap','like',"%".$cari."%")->get();
        $team=team::where('nama_lengkap','like',"%".$cari."%")->get();
        $kategori_team=kategori_teams::all();
        // mengirim data pegawai ke view index
        return view('admin.content.team',compact('team','kategori_team'));
	}
    public function tambah(){
        // $kategori_team=DB::table('kategori_teams')->get();
        $kategori_team=kategori_teams::all();
        return view('admin.content.add.add_team',compact('kategori_team'));
    }
    public function store(Request $request){
        $request->validate([
            'nama_panggilan'=>'required|unique:team',
            'nama_lengkap'=>'required|unique:team',
            'tempat_lahir'=>'required',
            'tanggal_lahir'=>'required',
            'email'=>'required',
            'no_hp'=>'required',
            'alamat'=>'required',
            'gambar'=>'required',
            'jabatan'=>'required',
            'kategori_team'=>'required',
        ]);
        $query=DB::table('team')->insert(
            [
                "nama_panggilan"=>$request['nama_panggilan'],
                "nama_lengkap"=>$request['nama_lengkap'],
                "tempat_lahir"=>$request['tempat_lahir'],
                "tanggal_lahir"=>$request['tanggal_lahir'],
                "email"=>$request['email'],
                "no_hp"=>$request['no_hp'],
                "alamat"=>$request['alamat'],
                "gambar"=>$request['gambar'],
                "jabatan"=>$request['jabatan'],
                "kategori_team"=>$request['kategori_team']
            ]
        );
        return redirect('/admin/team')->with('sukses','Yee selamat data Berhasil Disimpan');
    }
    public function detail($id){
        // $team=DB::table('team')->where('id',$id)->first();
        $kategori_team=kategori_teams::all();
        $team=team::find($id);
        return view('admin.content.detail.detail_team',compact('team','kategori_team'));
    }
    public function delate($id){
        // $query=DB::table('team')->where('id',$id)->delete();
        team::destroy($id);
        return redirect('/admin/team')->with('sukses','Data Anda Berhasil Dihapus');
    }
    public function edit($id){
        // $team=DB::table('team')->where('id',$id)->first();
        // $kategori_team=DB::table('kategori_teams')->get();
        $team=team::find($id);
        $kategori_team=kategori_teams::all();
        return view('admin.content.edit.edit_team',compact('team','kategori_team'));
    }
    public function update($id,Request $request){
        $request->validate([
            'nama_panggilan'=>'required',
            'nama_lengkap'=>'required',
            'tempat_lahir'=>'required',
            'tanggal_lahir'=>'required',
            'email'=>'required',
            'no_hp'=>'required',
            'alamat'=>'required',
            'gambar'=>'required',
            'jabatan'=>'required',
            'kategori_team'=>'required',
        ]);
        // $query=DB::table('team')->where('id',$id)->update([
        //     "nama_panggilan"=>$request['nama_panggilan'],
        //     "nama_lengkap"=>$request['nama_lengkap'],
        //     "tempat_lahir"=>$request['tempat_lahir'],
        //     "tanggal_lahir"=>$request['tanggal_lahir'],
        //     "email"=>$request['email'],
        //     "no_hp"=>$request['no_hp'],
        //     "alamat"=>$request['alamat'],
        //     "gambar"=>$request['gambar'],
        //     "jabatan"=>$request['jabatan'],
        //     "kategori_team"=>$request['kategori_team']
        // ]);
        $update=team::where('id',$id)->update([
            "nama_panggilan"=>$request['nama_panggilan'],
            "nama_lengkap"=>$request['nama_lengkap'],
            "tempat_lahir"=>$request['tempat_lahir'],
            "tanggal_lahir"=>$request['tanggal_lahir'],
            "email"=>$request['email'],
            "no_hp"=>$request['no_hp'],
            "alamat"=>$request['alamat'],
            "gambar"=>$request['gambar'],
            "jabatan"=>$request['jabatan'],
            "kategori_team"=>$request['kategori_team']
        ]);
        return redirect('/admin/team')->with('sukses','Yee selamat data Berhasil Diupdate');
    }
}
