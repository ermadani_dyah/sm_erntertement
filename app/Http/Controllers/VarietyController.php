<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\variety;
class VarietyController extends Controller
{
    public function index(){
        // $variety=DB::table('variety')->get();
        $variety=variety::all();
        return view('admin.content.variety',compact('variety'));
    }
    public function cari(Request $request){
		// menangkap data pencarian
		$cari = $request->cari;
 
        // mengambil data dari table pegawai sesuai pencarian data
        // $variety=DB::table('variety')->where('program','like',"%".$cari."%")->get();
        $variety=variety::where('program','like',"%".$cari."%")->get();
        // mengirim data pegawai ke view index
        return view('admin.content.variety',compact('variety'));
	}
    public function tambah(){
        return view('admin.content.add.add_variety');
    }
    public function store(Request $request){
        $request->validate([
            'program'=>'required|unique:variety',
            'penayangan'=>'required',
            'gambar'=>'required',
        ]);
        // $query=DB::table('variety')->insert(
        //     [
        //         "program"=>$request['program'],
        //         "penayangan"=>$request['penayangan'],
        //         "gambar"=>$request['gambar'],
        //     ]
        // );
        $variety=variety::create([
            "program"=>$request['program'],
            "penayangan"=>$request['penayangan'],
            "gambar"=>$request['gambar'],
        ]);
        return redirect('/admin/variety')->with('sukses','Yee selamat data Berhasil Disimpan');
    }
    public function delate($id){
        // $query=DB::table('variety')->where('id',$id)->delete();
        variety::destroy($id);
        return redirect('/admin/variety')->with('sukses','Data Anda Berhasil Dihapus');
    }
    public function edit($id){
        // $variety=DB::table('variety')->where('id',$id)->first();
        $variety=variety::find($id);
        return view('admin.content.edit.edit_variety',compact('variety'));
    }
    public function update($id,Request $request){
        $request->validate([
            'program'=>'required',
            'penayangan'=>'required',
            'gambar'=>'required',
        ]);
        // $query=DB::table('variety')->where('id',$id)->update([
        //     "program"=>$request['program'],
        //     "penayangan"=>$request['penayangan'],
        //     "gambar"=>$request['gambar'],
        // ]);
        $update=variety::where('id',$id)->update([
            "program"=>$request['program'],
            "penayangan"=>$request['penayangan'],
            "gambar"=>$request['gambar'],
        ]);
        return redirect('/admin/variety')->with('sukses','Yee selamat data Berhasil Diupdate');
    }
}
