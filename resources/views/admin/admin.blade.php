<!doctype html>
<html lang="en">
    <!--Head-->
    @include('admin.include.head') 
    <!--End Head-->
    <body>
        <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
            <!--Header-->
            @include('admin.include.header') 
            <!--End Header-->
            <div class="app-main">
                <!--Sidebar-->
                @include('admin.include.sidebar') 
                <!--End Sidebar-->    
                <div class="app-main__outer">
                    <!--Content-->
                    @yield('content')
                    <!--End Content-->

                    <!--Footer-->
                    @include('admin.include.footer')
                    <!--End Footer-->
                </div>
                <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
            </div>
        </div>
    <script type="text/javascript" src="{{asset('/assets/scripts/main.js')}}"></script>
    </body>
</html>
