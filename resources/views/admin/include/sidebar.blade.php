<div class="app-sidebar sidebar-shadow">
    <div class="app-header__logo">
        <div class="logo-src"></div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>    <div class="scrollbar-sidebar">
        <div class="app-sidebar__inner">
            <ul class="vertical-nav-menu">
                <li class="app-sidebar__heading">Main</li>
                <li>
                    <a href="/admin/dashboard">
                        <i class="metismenu-icon pe-7s-users"></i>
                        Dashboard
                    </a>
                </li>
                <li class="app-sidebar__heading">Home</li>
                <li>
                    <a href="/admin/team">
                        <i class="metismenu-icon pe-7s-users"></i>
                        Team
                    </a>
                </li>
                <li>
                    <a href="/admin/companies">
                        <i class="metismenu-icon pe-7s-map"></i>
                        Group Companies
                    </a>
                </li>
                <li>
                    <a href="/admin/business">
                        <i class="metismenu-icon pe-7s-note2"></i>
                        Business Unit
                    </a>
                </li>
                <li class="app-sidebar__heading">Outher</li>
                <li>
                    <a href="/admin/actris">
                        <i class="metismenu-icon pe-7s-user-female"></i>
                        Actris
                    </a>
                </li>
                <li>
                    <a href="/admin/album">
                        <i class="metismenu-icon pe-7s-notebook"></i>
                        Album
                    </a>
                </li>
                <li>
                    <a href="/admin/variety">
                        <i class="metismenu-icon pe-7s-video"></i>
                        Variety Show
                    </a>
                </li>
                <li>
                    <a href="/admin/movie">
                        <i class="metismenu-icon pe-7s-monitor"></i>
                        Movie/Drama
                    </a>
                </li>
                <li class="app-sidebar__heading">Admin</li>
                <li>
                    <a href="/admin/user">
                        <i class="metismenu-icon pe-7s-add-user"></i>
                        User
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>