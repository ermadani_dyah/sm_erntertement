@extends('admin.admin')

@section('content')
    <div class="app-main__inner">  
        <div class="row">
            <div class="col-md-12">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h4 class="m-0">Add Variety Show</h4>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item">
                                        <a href="/admin/dashboard">Home</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="/admin/variety">Variety Show</a>
                                    </li>
                                    <li class="breadcrumb-item active">Add Variety Show</li>
                                </ol>
                            </div>
                            <!-- /.col -->
                        </div>
                    <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <div class="main-card mb-3 card">
                    <div class="card-header">
                        Add Variety Show
                    </div>
                    <div class="card-body">
                        <form class="form-horizontal" enctype="multipart/form-data" method="post" action="/admin/variety">
                            @csrf
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="L_program" class="">Program</label>
                                        @error('program')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                        <input name="program" id="program"  type="text" class="form-control" value="{{old('program','')}} ">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="L_penayangan" class="">Penayangan</label>
                                        @error('penayangan')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                        <input name="penayangan" id="penayangan"  type="text" class="form-control" value="{{old('penayangan','')}} ">
                                    </div>
                                </div>
                            </div>
                            <div class="position-relative form-group">
                                <label for="L_gambar" class="">Gambar</label>
                                @error('gambar')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <input name="gambar" id="gambar"  type="text" class="form-control" value="{{old('gambar','')}} ">
                            </div>
                            <div class="d-block text-center card-footer">
                                <a href="/admin/variety" class="btn btn-warning float-left">
                                    <i class="pe-7s-angle-left-circle btn-icon-wrapper"> </i>
                                    Back
                                </a>
                                <button type="submit" class="btn btn-success float-right" name="submit"  value="Submit">
                                    <i class="pe-7s-plus btn-icon-wrapper"> </i>
                                    Add 
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> 
@endsection