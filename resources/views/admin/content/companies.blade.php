@extends('admin.admin')

@section('content')
<div class="app-main__inner">  
    <div class="row">
        <div class="col-md-12">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h4 class="m-0">Group Companies</h4>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item">
                                    <a href="/admin/dashboard">Home</a>
                                </li>
                                <li class="breadcrumb-item active">Group Companies</li>
                            </ol>
                        </div>
                        <!-- /.col -->
                    </div>
                <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <div class="main-card mb-3 card">
                <div class="card-header">
                    List Group Companies
                    <div class="btn-actions-pane-right">
                        <div role="group" class="btn-group-sm btn-group">
                            <a href="/admin/companies/add_companies" class="btn-wide btn btn-success">
                                <i class="pe-7s-plus btn-icon-wrapper"> </i>
                                Add List Group Companies
                            </a>
                        </div>
                    </div>
                </div>
                <br>
                <div class="col-md-12">
                    <form method="GET" action="/admin/companies/cari">
                        <div class="row">
                            <div class="col-md-4 bottom-10">
                                <input type="text" class="form-control" id="cari" name="cari" placeholder="Nama Companies" value="{{ old('cari') }}">
                            </div>
                            <div class="col-md-5 bottom-10">
                                <input type="submit" value="Search" class="btn btn-primary"/>
                            </div>
                        </div>
                        <!-- .row -->
                    </form>
                </div>
                <br>
                @if(session('sukses'))
                    <div class="col-sm-12">
                        <div class="alert alert-success">
                            {{session('sukses')}}
                        </div>
                    </div>
                @endif
                <div class="table-responsive">
                    <table class="align-middle mb-0 table table-striped table-hover">
                        <thead>
                            <tr>
                                <th class="text-center">No</th>
                                <th class="text-center">Nama</th>
                                <th class="text-center">Alamat</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($companies as $key=>$companiess)
                                <tr>
                                    <td class="text-center text-muted">{{$key+1}}</td>
                                    <td class="text-center">{{$companiess->nama_companies}}</td>
                                    <td class="text-center">{{$companiess->alamat}}</td>
                                    <td class="text-center">
                                        <a href="/admin/companies/{{$companiess->id}}/edit" class="mr-2 btn-icon btn-icon-only btn btn-outline-warning">
                                            <i class="pe-7s-note btn-icon-wrapper"> </i>
                                            Edit
                                        </a>
                                        <form action="/admin/companies/{{$companiess->id}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <input type="submit" value="delete" class="mr-2 btn-icon btn-icon-only btn btn-outline-danger">
                                        </form> 
                                        <a href="/admin/companies/{{$companiess->id}}" class="mr-2 btn-icon btn-icon-only btn btn-outline-info">
                                            <i class="pe-7s-look btn-icon-wrapper"> </i>
                                            Details 
                                        </a> 
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="5" align="center">No Post</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <div class="d-block text-center card-footer"></div>
            </div>
        </div>
    </div>
</div>
@endsection