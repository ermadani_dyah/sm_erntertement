@extends('admin.admin')

@section('content')
    <div class="app-main__inner">  
        <div class="row">
            <div class="col-md-12">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h4 class="m-0">Edit Companies</h4>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item">
                                        <a href="/admin/dashboard">Home</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="/admin/companies">Companies</a>
                                    </li>
                                    <li class="breadcrumb-item active">Edit Companies</li>
                                </ol>
                            </div>
                            <!-- /.col -->
                        </div>
                    <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <div class="main-card mb-3 card">
                    <div class="card-header">
                        Edit Companies
                    </div>
                    <div class="card-body">
                        <form class="form-horizontal" enctype="multipart/form-data" method="post" action="/admin/companies/{{$companies->id}}">
                            @csrf
                            @method('PUT')
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="L_nama_companies" class="">Nama</label>
                                        @error('nama_companies')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                        <input name="nama_companies" id="nama_companies"  type="text" class="form-control" value="{{old('nama_companies',$companies->nama_companies)}} ">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="L_alamat" class="">Alamat</label>
                                        @error('alamat')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                        <input name="alamat" id="alamat"  type="text" class="form-control" value="{{old('alamat',$companies->alamat)}} ">
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="L_email" class="">Email</label>
                                        @error('email')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                        <input name="email" id="email"  type="email" class="form-control" value="{{old('email',$companies->email)}} ">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="L_no_hp" class="">No Hp</label>
                                        @error('no_hp')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                        <input name="no_hp" id="no_hp" type="number" class="form-control" value="{{old('no_hp',$companies->no_hp)}} ">
                                    </div>
                                </div>
                            </div>
                            <div class="position-relative form-group">
                                <label for="L_ceo" class="">CEO</label>
                                @error('ceo')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <input name="ceo" id="ceo"  type="text" class="form-control" value="{{old('ceo',$companies->ceo)}} ">
                            </div>
                            <div class="position-relative form-group">
                                <label for="hobi" class="">Gambar</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" name="gambar" id="gambar" value="{{old('gambar',$companies->gambar)}} ">
                                    <label class="custom-file-label" for="customFile">{{$companies->gambar}}</label>
                                </div>  
                            </div>
                            <div class="d-block text-center card-footer">
                                <a href="/admin/companies" class="btn btn-warning float-left">
                                    <i class="pe-7s-angle-left-circle btn-icon-wrapper"> </i>
                                    Back
                                </a>
                                <button type="submit" class="btn btn-success float-right" name="submit"  value="Submit">
                                    <i class="pe-7s-pen btn-icon-wrapper"> </i>
                                    Edit 
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection