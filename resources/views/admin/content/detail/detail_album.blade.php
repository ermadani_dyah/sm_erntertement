@extends('admin.admin')

@section('content')
    <div class="app-main__inner">  
        <div class="row">
            <div class="col-md-12">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h4 class="m-0">Details Album</h4>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item">
                                        <a href="/admin/dashboard">Home</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="/admin/album">Album</a>
                                    </li>
                                    <li class="breadcrumb-item active">Details Album</li>
                                </ol>
                            </div>
                            <!-- /.col -->
                        </div>
                    <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <div class="main-card mb-3 card">
                    <div class="card-header">
                        Details Album
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <tbody>  
                                <tr>
                                    <td colspan="2">
                                        <i class="mr-3 fas fa-book"></i>
                                        <strong>Detail Album</strong>
                                    </td>
                                </tr>               
                                <tr>
                                    <td width="20%">
                                        <strong>Album</strong>
                                    </td>
                                    <td width="80%">
                                        {{$album->nama_album}}
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <strong>Penyanyi</strong>
                                    </td>
                                    <td width="80%">
                                        {{$album->nama_penyanyi}}
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <strong>Album Type</strong>
                                    </td>
                                    <td width="80%">
                                        {{$album->album_type}}
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <strong>Genre</strong>
                                    </td>
                                    <td width="80%">
                                        {{$album->genre}}
                                    </td>  
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <strong>Record Label</strong>
                                    </td>
                                    <td width="80%">
                                        {{$album->record_label}}
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <strong>Distribution</strong>
                                    </td>
                                    <td width="80%">
                                        {{$album->distribution}}
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <strong>Release</strong>
                                    </td>
                                    <td width="80%">
                                        {{$album->release}}
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <strong>Foto</strong>
                                    </td>
                                    <td width="80%">
                                        <img width="300"  src="{{$album->gambar}}" alt="">
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <strong>Link</strong>
                                    </td>
                                    <td width="80%">
                                        {{$album->link}}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="d-block text-center card-footer">
                        <a href="/admin/album" class="btn btn-warning float-right">
                            <i class="pe-7s-angle-left-circle btn-icon-wrapper"> </i>
                            Back
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection