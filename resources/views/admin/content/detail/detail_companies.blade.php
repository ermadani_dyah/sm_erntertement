@extends('admin.admin')

@section('content')
    <div class="app-main__inner">  
        <div class="row">
            <div class="col-md-12">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h4 class="m-0">Details Companies</h4>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item">
                                        <a href="/admin/dashboard">Home</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="/admin/companies">Companies</a>
                                    </li>
                                    <li class="breadcrumb-item active">Details Companies</li>
                                </ol>
                            </div>
                            <!-- /.col -->
                        </div>
                    <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <div class="main-card mb-3 card">
                    <div class="card-header">
                        Details Companies
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <tbody>  
                                <tr>
                                    <td colspan="2">
                                        <i class="mr-3 fas fa-building"></i>
                                        <strong>Detail Companies</strong>
                                    </td>
                                </tr>               
                                <tr>
                                    <td width="20%">
                                        <strong>Nama</strong>
                                    </td>
                                    <td width="80%">
                                        {{$companies->nama_companies}}
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <strong>Email</strong>
                                    </td>
                                    <td width="80%">
                                        {{$companies->email}}
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <strong>No Hp</strong>
                                    </td>
                                    <td width="80%">
                                        {{$companies->no_hp}}
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <strong>Alamat</strong>
                                    </td>
                                    <td width="80%">
                                        {{$companies->alamat}}
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <strong>CEO</strong>
                                    </td>
                                    <td width="80%">
                                        {{$companies->ceo}}
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <strong>Foto</strong>
                                    </td>
                                    <td width="80%">
                                        <img width="300"  src="{{ url('/uploads/'.$companies->gambar) }}" alt="">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="d-block text-center card-footer">
                        <a href="/admin/companies" class="btn btn-warning float-right">
                            <i class="pe-7s-angle-left-circle btn-icon-wrapper"> </i>
                            Back
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection