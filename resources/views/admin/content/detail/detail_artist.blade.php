@extends('admin.admin')

@section('content')
    <div class="app-main__inner">  
        <div class="row">
            <div class="col-md-12">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h4 class="m-0">Details Actris</h4>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item">
                                        <a href="/admin/dashboard">Home</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="/admin/actris">Actris</a>
                                    </li>
                                    <li class="breadcrumb-item active">Details Actris</li>
                                </ol>
                            </div>
                            <!-- /.col -->
                        </div>
                    <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <div class="main-card mb-3 card">
                    <div class="card-header">
                        Details Actris
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <tbody>  
                                <tr>
                                    <td colspan="2">
                                        <i class="mr-3 fas fa-users"></i>
                                        <strong>Detail Actris</strong>
                                    </td>
                                </tr>               
                                <tr>
                                    <td width="20%">
                                        <strong>Nama Lengkap</strong>
                                    </td>
                                    <td width="80%">
                                        {{$artis->nama_panjang}}
                                    </td>
                                </tr>                 
                                <tr>
                                    <td width="20%">
                                        <strong>Nama Panggilan</strong>
                                    </td>
                                    <td width="80%">
                                        {{$artis->nama_panggilan}}
                                    </td>
                                </tr>                 
                                <tr>
                                    <td width="20%">
                                        <strong>Tempat Lahir</strong>
                                    </td>
                                    <td width="80%">
                                        {{$artis->tempat_lahir}}
                                    </td>
                                </tr> 
                                <tr>
                                    <td width="20%">
                                        <strong>Tanggal Lahir</strong>
                                    </td>
                                    <td width="80%">
                                        {{$artis->tanggal_lahir}}
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <strong>Email</strong>
                                    </td>
                                    <td width="80%">
                                        {{$artis->email}}
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <strong>No Hp</strong>
                                    </td>
                                    <td width="80%">
                                        {{$artis->no_hp}}
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <strong>Alamat</strong>
                                    </td>
                                    <td width="80%">
                                        {{$artis->alamat}}
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <strong>Kategori 1</strong>
                                    </td>
                                    <td width="80%">
                                        @foreach ($kategori_artis as $key=>$kategori_artiss)
                                            @if($kategori_artiss->id== $artis->kategori1)
                                                {{ $kategori_artiss->nama_kategori_actris }}
                                            @endif 
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <strong>Kategori 2</strong>
                                    </td>
                                    <td width="80%">
                                        @foreach ($kategori_artis as $key=>$kategori_artiss)
                                            @if($kategori_artiss->id== $artis->kategori2)
                                                {{ $kategori_artiss->nama_kategori_actris }}
                                            @endif 
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <strong>Kategori 3</strong>
                                    </td>
                                    <td width="80%">
                                        @foreach ($kategori_artis as $key=>$kategori_artiss)
                                            @if($kategori_artiss->id== $artis->kategori3)
                                                {{ $kategori_artiss->nama_kategori_actris }}
                                            @endif 
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <strong>Link</strong>
                                    </td>
                                    <td width="80%">
                                        {{$artis->link}}
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <strong>Foto</strong>
                                    </td>
                                    <td width="80%">
                                        <img width="300"  src="{{$artis->gambar}}" alt="">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="d-block text-center card-footer">
                        <a href="/admin/actris" class="btn btn-warning float-right">
                            <i class="pe-7s-angle-left-circle btn-icon-wrapper"> </i>
                            Back
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection