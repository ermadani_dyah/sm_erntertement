@extends('admin.admin')

@section('content')
    <div class="app-main__inner">  
        <div class="row">
            <div class="col-md-12">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h4 class="m-0">Business Unit</h4>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item">
                                        <a href="/admin/dashboard">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active">Business Unit</li>
                                </ol>
                            </div>
                            <!-- /.col -->
                        </div>
                    <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <div class="main-card mb-3 card">
                    <div class="card-header">
                        List Business Unit
                        <div class="btn-actions-pane-right">
                            <div role="group" class="btn-group-sm btn-group">
                                <a href="/admin/business/add_business" class="btn-wide btn btn-success">
                                    <i class="pe-7s-plus btn-icon-wrapper"> </i>
                                    Add List Business Unit
                                </a>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="col-md-12">
                        <form method="GET" action="/admin/business/cari">
                            <div class="row">
                                <div class="col-md-4 bottom-10">
                                    <input type="text" class="form-control" id="cari" name="cari" placeholder="Nama Business" value="{{ old('cari') }}">
                                </div>
                                <div class="col-md-5 bottom-10">
                                    <input type="submit" value="Search" class="btn btn-primary"/>
                                </div>
                            </div>
                            <!-- .row -->
                        </form>
                    </div>
                    <br>
                    @if(session('sukses'))
                        <div class="col-sm-12">
                            <div class="alert alert-success">
                                {{session('sukses')}}
                            </div>
                        </div>
                    @endif
                    <div class="table-responsive">
                        <table class="align-middle mb-0 table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center">No</th>
                                    <th class="text-center">Nama Business</th>
                                    <th class="text-center">Kategori</th>
                                    <th class="text-center">Jenis Bisnis</th>
                                    <th class="text-center">Gambar</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($business as $key=>$businesss)
                                    <tr>
                                        <td class="text-center text-muted">{{$key+1}}</td>
                                        <td class="text-center">{{$businesss->nama_business}}</td>
                                        <td class="text-center">
                                            @foreach ($kategori_business as $key=>$kategori_businesss)
                                                @if($kategori_businesss->id== $businesss->kategori_business)
                                                    {{ $kategori_businesss->nama_kategori_business }}, 
                                                @endif 
                                            @endforeach
                                        </td>
                                        <td class="text-center">{{$businesss->jenis_business}}</td>
                                        <td class="text-center">
                                            <img width="200"  src="{{ url('/uploads/'.$businesss->gambar) }}" alt="">
                                        </td>
                                        <td class="text-center">
                                            <a href="/admin/business/{{$businesss->id}}/edit" class="mr-2 btn-icon btn-icon-only btn btn-outline-warning">
                                                <i class="pe-7s-note btn-icon-wrapper"> </i>
                                                Edit
                                            </a>
                                            <form action="/admin/business/{{$businesss->id}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <input type="submit" value="delete" class="mr-2 btn-icon btn-icon-only btn btn-outline-danger">
                                            </form>
                                        </td>
                                    </tr>
                                    @empty
                                <tr>
                                    <td colspan="6" align="center">No Post</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                    <div class="d-block text-center card-footer"></div>
                </div>
            </div>
        </div>
    </div>
@endsection