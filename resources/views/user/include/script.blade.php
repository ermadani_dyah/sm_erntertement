<!-- Bootstrap -->
<script src="{{asset('/assets/js/bootstrap.bundle.min.js')}}"></script>
<!-- Load jQuery require for isotope -->
<script src="{{asset('/assets/js/jquery.min.js')}}"></script>
<!-- Isotope -->
<script src="{{asset('/assets/js/isotope.pkgd.js')}}"></script>
<!-- Page Script -->
<script>
    $(window).load(function() {
        // init Isotope
        var $projects = $('.projects').isotope({
            itemSelector: '.project',
            layoutMode: 'fitRows'
        });
        $(".filter-btn").click(function() {
            var data_filter = $(this).attr("data-filter");
            $projects.isotope({
                filter: data_filter
            });
            $(".filter-btn").removeClass("active");
            $(".filter-btn").removeClass("shadow");
            $(this).addClass("active");
            $(this).addClass("shadow");
            return false;
        });
    });
</script>
<!-- Templatemo -->
<script src="{{asset('/assets/js/templatemo.js')}}"></script>
<!-- Custom -->
<script src="{{asset('/assets/js/custom.js')}}"></script>